package org.nohope;

import java.io.Serializable;

/**
* @author <a href="mailto:ketoth.xupack@gmail.com">ketoth xupack</a>
* @since 10/7/12 1:57 AM
*/
public class ClientClass implements Serializable {
    private static final long serialVersionUID = 8899275995655350878L;

    @Override
    public String toString() {
        return "wow! this class is loaded over wire! " + new ClientParam();
    }
}
